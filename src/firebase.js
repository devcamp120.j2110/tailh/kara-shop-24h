import "firebase/auth";
import firebase from "firebase/app";


const firebaseConfig = {
  apiKey: "AIzaSyD3_iNEucoon3NLCb8S6pW18Xr6UIB5qFc",
  authDomain: "devcamp-f6178.firebaseapp.com",
  projectId: "devcamp-f6178",
  storageBucket: "devcamp-f6178.appspot.com",
  messagingSenderId: "900707100661",
  appId: "1:900707100661:web:9dd544dcf298d32112e2bc",
};

firebase.initializeApp(firebaseConfig);
export const auth = firebase.auth();
export const googleProvider = new firebase.auth.GoogleAuthProvider();
