// Page
import HeaderComponent from "./components/Header/HeaderComponent";
import HomePage from "./components/Content/HomePage/HomePage";
import SignUp from "./components/Content/SignUp/SignUp";
import AllProduct from "./components/Content/AllProduct/AllProduct";
import FooterComponent from "./components/Footer/FooterComponent";
import ProductDetail from "./components/Content/ProductDetail/ProductDetail";
import ShoppingCart from "./components/Content/ShoppingCart/ShoppingCart";
import AccountCreated from "./components/Content/AccountCreated/AccountCreated";
import OrderSuccessful from "./components/Content/OrderSuccessful/OrderSuccessful";
// CSS Bootstrap
import "bootstrap/dist/css/bootstrap.css";
// Router
import { Routes, Route, useNavigate, useParams } from "react-router-dom";
// Hook
import { useEffect, useState } from "react";
// Fetch API
import fetchApi from "../src/fetchApi";
// Material UI
import { Grid } from "@mui/material";

function App() {
  // State
  const [productDataHomePage, setProductDataHomePage] = useState("");
  const [productList, setProductList] = useState("");
  const [countProduct, setCountProduct] = useState("");
  const [productDetail, setProductDetail] = useState("");
  const [productRelated, setProductRelated] = useState("");
  // Pagination
  const [limit, setLimit] = useState(6);
  const [numberOfPage, setNumberOfPage] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [filterObj, setFilterObj] = useState({
    name: "",
    type: "",
    color: "",
    minPrice: "",
    maxPrice: "",
  });
  // Get Product Home Page
  const getProductHomePage = (paramLimit) => {
    fetchApi("http://localhost:8080/products" + "?limit=" + paramLimit)
      .then((data) => {
        console.log(data);
        // Lưu tất cả products vào state
        setProductDataHomePage(data.products);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  //Get Product Count
  const getProductCount = () => {
    fetchApi("http://localhost:8080/products/count")
      .then((data) => {
        console.log(data.number);
        setCountProduct(data.number); //Lưu tổng số lượng products vào state
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Get All Product
  const getProductList = (paramLimit, paramSkip) => {
    fetchApi(
      "http://localhost:8080/products" +
        "?limit=" +
        paramLimit +
        "&skip=" +
        paramSkip
    )
      .then((data) => {
        console.log(data);
        setProductList(data.products);
        setNumberOfPage(Math.ceil(countProduct / limit));
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Filter Product - Hàm Lọc Sản Phẩm
  // Input Object Filter
  const handleFilterProduct = (paramLimit, paramSkip, objectFilter) => {
    fetchApi(
      "http://localhost:8080/products/filter" +
        "?limit=" +
        "" +
        "&skip=" +
        "" +
        "&name=" +
        objectFilter.name.trim() +
        "&type=" +
        objectFilter.type +
        "&color=" +
        objectFilter.color +
        "&minPrice=" +
        objectFilter.minPrice.trim() +
        "&maxPrice=" +
        objectFilter.maxPrice.trim()
    )
      .then((data) => {
        console.log(data.products);
        // Reload Giao diện
        if (data.products.length > limit) {
          setNumberOfPage(Math.ceil(1));
          setProductList(data.products);
        } else {
          setNumberOfPage(Math.ceil(data.products.length / limit));
          setProductList(data.products);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Get Related Product - Hàm lấy các sản phẩm liên quan
  // Input Product Type
  const getProductRelated = (paramType) => {
    // Fetch Api get related products
    fetchApi("http://localhost:8080/products/related" + "?type=" + paramType)
      .then((data) => {
        console.log(data);
        setProductRelated(data.products);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Select Product - Hàm Select Product khi nhấn view
  // Naviage đến trang detail
  const { id } = useParams();
  const navigate = useNavigate();
  const selectedProduct = (product) => {
    // Fetch Api get 1 product
    fetchApi("http://localhost:8080/products/" + product._id)
      .then((data) => {
        console.log(data);
        setProductDetail(data.product);
      })
      .catch((err) => {
        console.log(err);
      });
    // Navigate đến trang details
    if (id === product._id) {
      navigate(`/product-detail/${product._id}`);
    } else {
      navigate(`/product-detail/${product._id}`);
    }
    // Scroll đến đầu trang
    if (window.scrollY) {
      window.scroll(0, 0);
    }
  };

  // Hook useEffect được gọi lại khi state dependencies thay đổi
  useEffect(() => {
    getProductCount();
    getProductHomePage(6);
    getProductList(limit, (currentPage - 1) * limit);
    console.log(productDataHomePage);
    console.log(productList);
  }, [currentPage, countProduct]);

  return (
    <div>
      {/* Header */}
      <HeaderComponent></HeaderComponent>
      {/* Router Content */}
      <div style={{ marginTop: "100px", marginBottom: "40px" }}>
        <Routes>
          {/* Home Page */}
          <Route
            exact
            path="/"
            element={
              <HomePage
                productData={productDataHomePage}
                selectedProduct={selectedProduct}
                getProductRelated={getProductRelated}
              ></HomePage>
            }
          ></Route>
          <Route
            exact
            path="*"
            element={
              <HomePage
                productData={productDataHomePage}
                selectedProduct={selectedProduct}
                getProductRelated={getProductRelated}
              ></HomePage>
            }
          ></Route>
          {/* All Product Page */}
          <Route
            exact
            path="/allproduct"
            element={
              <AllProduct
                productList={productList}
                setProductList={setProductList}
                numberOfPage={numberOfPage}
                currentPage={currentPage}
                setCurrentPage={setCurrentPage}
                limit={limit}
                filterObj={filterObj}
                setFilterObj={setFilterObj}
                handleFilterProduct={handleFilterProduct}
                selectedProduct={selectedProduct}
                getProductRelated={getProductRelated}
                getProductList={getProductList}
              ></AllProduct>
            }
          ></Route>
          {/* Detail Page */}
          <Route
            exact
            path="/product-detail/:id"
            element={
              <ProductDetail
                productDetail={productDetail}
                productRelated={productRelated}
                getProductRelated={getProductRelated}
                selectedProduct={selectedProduct}
              />
            }
          ></Route>
          {/* Shopping Cart */}
          <Route
            exact
            path="/shopping-cart"
            element={
              <Grid container justifyContent="center">
                <ShoppingCart></ShoppingCart>
              </Grid>
            }
          ></Route>
          {/* Order Successful */}
          <Route
            exact
            path="/order-successful"
            element={<OrderSuccessful />}
          ></Route>
          {/* Sign Up */}
          <Route exact path="/sign-up" element={<SignUp />}></Route>
          {/* Account Created */}
          <Route
            exact
            path="/account-created"
            element={<AccountCreated />}
          ></Route>
        </Routes>
      </div>
      {/* Footer */}
      <FooterComponent></FooterComponent>
    </div>
  );
}

export default App;
