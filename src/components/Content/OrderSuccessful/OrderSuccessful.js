// Import IMG
import CartSuccessPng from "../../../assets/images/cart-success.png";
// Material UI
import { Grid, Typography, Button } from "@mui/material";
// Router
import { useNavigate } from "react-router-dom";
// Style MUI
import { styled } from "@mui/material/styles";
import { orange } from "@mui/material/colors";
// Icon
import HomeIcon from "@mui/icons-material/Home";
function OrderSuccessful() {
  // Navigate
  const navigate = useNavigate();
  const goToHomePage = () => {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    navigate("/");
  };
  // Custom Button
  const ColorButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(orange[400]),
    backgroundColor: orange[400],
    "&:hover": {
      backgroundColor: orange[500],
    },
  }));
  return (
    <Grid container justifyContent="center">
      <Grid className="mt-3" align="center" item xs={12}>
        <img width="200" height="200" src={CartSuccessPng} alt="cart-succes" />
      </Grid>
      <Grid className="mt-3" align="center" item xs={12}>
        <Typography fontWeight="bold" variant="h3" gutterBottom component="div">
          Thanks for your order!
        </Typography>
        <Typography
          fontWeight="bold"
          variant="body1"
          gutterBottom
          component="div"
        >
          Your order made our day! We hope we make yours.
          <br />
          If you have any questions about your order, contact us anytime.
          <br />
          We'd love to hear from you
        </Typography>
      </Grid>
      <Grid className="mt-3" align="center" item xs={12}>
        {/* Go To Home Page Button */}
        <ColorButton
          onClick={goToHomePage}
          className="fw-bold"
          variant="contained"
          size="large"
          startIcon={<HomeIcon />}
        >
          Go To Home Page
        </ColorButton>
      </Grid>
    </Grid>
  );
}

export default OrderSuccessful;
