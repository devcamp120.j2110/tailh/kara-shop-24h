// Material UI
import { Grid } from "@mui/material";
// Component
import ProductInfo from "./ProductInfo/ProductInfo";
import ProductDescription from "./ProductDescription/ProductDescription";
import ProductRelated from "./ProductRelated/ProductRelated";
import BreadcrumbComponent from "../../Breadcrumb/BreadcrumbComponent";


function ProductDetail(props) {
  return (
    <Grid justifyContent="center" container>
      <Grid item xs={12}>
        {/* Breadcrumb */}
        <BreadcrumbComponent productDetail={props.productDetail}></BreadcrumbComponent>
      </Grid>
      {/* Product Info */}
      <Grid item xs={10} mt={3}>
        <ProductInfo productDetail={props.productDetail}></ProductInfo>
      </Grid>
      {/* Product Description */}
      <Grid item xs={10} mt={3}>
        <ProductDescription
          productDetail={props.productDetail}
        ></ProductDescription>
      </Grid>
      {/* Products Related*/}
      <Grid item xs={10} mt={3}>
        <ProductRelated
          productDetail={props.productDetail}
          productRelated={props.productRelated}
          selectedProduct={props.selectedProduct}
          getProductRelated={props.getProductRelated}
        ></ProductRelated>
      </Grid>
    </Grid>
  );
}

export default ProductDetail;
