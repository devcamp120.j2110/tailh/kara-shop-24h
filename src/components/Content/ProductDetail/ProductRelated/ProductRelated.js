// Material UI
import {
  Grid,
  Typography,
  Card,
  CardMedia,
  CardContent,
  CardActions,
  Button,
} from "@mui/material";
// Icon
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
// Redux
import { useDispatch } from "react-redux";
import { add } from "../../../../redux/Cart/CartSlice";
// Component
import SnackBar from "../../../SnackBar/SnackBarComponent";
// Hook
import { useState } from "react";

function ProductRelated(props) {
  // State
  const [snackBar, setSnackBar] = useState({
    display: false,
    type: "",
    text: "",
  });
  // Hàm xử lý khi nhấn nút View
  const viewDetailProduct = (product) => {
    // select product và navigate đến trang detail
    props.selectedProduct(product);
    // Lọc ra sản phẩm liên quan
    props.getProductRelated(product.type);
  };
  // Redux
  const dispatch = useDispatch();
  // Hàm xử lý khi nhấn nút add
  const addToCart = (product, quantity) => {
    // gọi dispatch để update cartSlice
    dispatch(add({ product: product, quantity: quantity }));
    // Hiện SnackBar
    setSnackBar({
      display: true,
      type: "success",
      text: "Product Added To Cart",
    });
  };
  return (
    <Grid container>
      <Grid item xs={12}>
        <Typography variant="h5" fontWeight="bold" gutterBottom component="div">
          Product Related
        </Typography>
      </Grid>
      <Grid container xs={12} spacing={4}>
        {props.productRelated !== ""
          ? props.productRelated.map((product, index) => {
              return (
                <Grid item xs={3} key={index}>
                  <Card sx={{ maxWidth: "100%" }} className="p-4">
                    <CardMedia
                      component="img"
                      image={require(`../../../../assets/images/${product.imageUrl}`)}
                      alt="green iguana"
                      style={{ objectFit: "contain", height: "220px" }}
                    />
                    <CardContent className="px-3 pt-3 pb-2">
                      <Typography
                        sx={{
                          fontWeight: "bold",
                          height: "44px",
                          overflow: "hidden",
                        }}
                        gutterBottom
                        variant="h4"
                        component="div"
                        className="cardHeader"
                      >
                        {product.name}
                      </Typography>
                      <Typography
                        sx={{
                          height: "80px",
                          overflow: "hidden",
                        }}
                        variant="body2"
                        color="text.secondary"
                      >
                        {product.description}
                      </Typography>
                      <Typography
                        className="m-0 d-inline text-decoration-line-through"
                        variant="h6"
                        gutterBottom
                        component="div"
                      >
                        {product.buyPrice}$
                      </Typography>
                      <Typography
                        className="m-0 fw-bold d-inline ms-1 price-text"
                        variant="h5"
                        gutterBottom
                        component="div"
                      >
                        {product.promotionPrice}$
                      </Typography>
                    </CardContent>
                    <CardActions>
                      <Button
                        className="fw-bold"
                        size="medium"
                        variant="outlined"
                        onClick={() => {
                          viewDetailProduct(product);
                        }}
                      >
                        View
                      </Button>
                      <Button
                        className="fw-bold"
                        size="medium"
                        variant="contained"
                        startIcon={<AddShoppingCartIcon />}
                        onClick={() => {
                          addToCart(product);
                        }}
                      >
                        Add To Cart
                      </Button>
                    </CardActions>
                  </Card>
                </Grid>
              );
            })
          : null}
      </Grid>
      {/* Snack Bar */}
      <SnackBar snackBar={snackBar} setSnackBar={setSnackBar}></SnackBar>
    </Grid>
  );
}

export default ProductRelated;
