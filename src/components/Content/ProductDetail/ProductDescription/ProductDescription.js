// Material UI
import { Grid, Typography } from "@mui/material";
function ProductDescription(props) {
  return (
    <Grid container>
      <Grid item xs={12}>
        <Typography variant="h5" fontWeight="bold" gutterBottom component="div">
          Description
        </Typography>
        <Typography
          variant="h6"
          gutterBottom
          component="div"
          sx={{ color: "text.secondary" }}
        >
          {props.productDetail.description
            ? props.productDetail.description +
              props.productDetail.description +
              props.productDetail.description +
              props.productDetail.description
            : null}
          {}
        </Typography>
      </Grid>
      <Grid align="center" item xs={12}>
        {props.productDetail.imageUrl ? (
          <img
            className="my-5"
            src={require(`../../../../assets/images/${props.productDetail.imageUrl}`)}
            alt={`Product Img`}
          />
        ) : null}
      </Grid>
    </Grid>
  );
}

export default ProductDescription;
