// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
// Style
import "./styles.css";
// import required modules
import { Pagination, Navigation } from "swiper";
function SliderDetail(props) {
  return (
    <div style={{ width:'90%' , height: "400px" }}>
      <Swiper
        slidesPerView={1}
        spaceBetween={30}
        loop={true}
        pagination={{
          clickable: true,
        }}
        navigation={true}
        modules={[Pagination, Navigation]}
        className="mySwiper"
      >
        {props.thumbnailUrl
          ? props.thumbnailUrl.map((thumbnail, index) => {
              return (
                <SwiperSlide key={index}>
                  <img
                    src={require(`../../../../../assets/images/${thumbnail}`)}
                    alt={`Product Img`}
                    style={{width: '600px',height: '200px',objectFit:"cover"}}
                  />
                </SwiperSlide>
              );
            })
          : null}
      </Swiper>
    </div>
  );
}

export default SliderDetail;
