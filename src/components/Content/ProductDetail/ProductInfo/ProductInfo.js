// Material UI
import { Grid, Typography, Button, Box } from "@mui/material";
// MUI Icon
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import RemoveCircleIcon from "@mui/icons-material/RemoveCircle";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import IconButton from "@mui/material/IconButton";
// Component
import SliderDetail from "./SliderDetail/SliderDetail";
import SnackBar from "../../../SnackBar/SnackBarComponent";
// Hook
import { useState } from "react";
// Redux
import { add } from "../../../../redux/Cart/CartSlice";
import { useDispatch } from "react-redux";

function ProductInfo(props) {
  // State
  const [quantity, setQuantity] = useState(1);
  const [snackBar, setSnackBar] = useState({
    display: false,
    type: "",
    text: "",
  });
  // Redux
  const dispatch = useDispatch();
  // Hàm xử lý khi nhấn nút add
  const addToCart = (product, quantity) => {
    // Gọi Hàm dispatch để update cartSlice
    dispatch(add({ product: product, quantity: quantity }));
    // Hiện SnackBar
    setSnackBar({
      display: true,
      type: "success",
      text: "Product Added To Cart",
    });
  };
  // Hàm tăng quantity lên 1 đơn vị
  const increment = () => {
    setQuantity(quantity + 1);
  };
  // Hàm giảm quantity xuống 1 đơn vị và quantity phải > 1
  const decrement = () => {
    if (quantity > 1) {
      setQuantity(quantity - 1);
    }
  };
  return (
    <Grid justifyContent="center" container>
      {/* Slider */}
      <Grid item xs={6}>
        <Grid justifyContent="center" container>
          <Grid item xs={12}>
            <SliderDetail
              thumbnailUrl={props.productDetail.thumbnailUrl}
            ></SliderDetail>
          </Grid>
        </Grid>
      </Grid>
      {/* Info */}
      <Grid item xs={6}>
        {/* Name */}
        <Typography
          mt={2}
          fontWeight="bold"
          variant="h2"
          gutterBottom
          component="div"
          sx={{ textTransform: "capitalize", color: "primary.main" }}
        >
          {props.productDetail.name !== "" ? props.productDetail.name : null}
        </Typography>
        {/* Type */}
        <Typography
          variant="h6"
          gutterBottom
          component="div"
          sx={{ color: "text.secondary" }}
        >
          Type:{" "}
          {props.productDetail.type !== "" ? (
            <span className="text-capitalize">
              {props.productDetail.type} Class
            </span>
          ) : null}
        </Typography>
        {/* Color */}
        <Typography
          variant="h6"
          gutterBottom
          component="div"
          sx={{ color: "text.secondary" }}
        >
          Color:{" "}
          {props.productDetail.color !== "" ? (
            <span className="text-capitalize">{props.productDetail.color}</span>
          ) : null}
        </Typography>
        {/* Description */}
        <Typography
          variant="h6"
          gutterBottom
          component="div"
          sx={{ color: "text.secondary" }}
        >
          {props.productDetail.description !== ""
            ? props.productDetail.description
            : null}
        </Typography>
        {/* Price */}
        <Typography
          fontWeight="bold"
          variant="h4"
          gutterBottom
          component="div"
          sx={{ color: "error.main" }}
        >
          {props.productDetail.promotionPrice !== ""
            ? props.productDetail.promotionPrice + "$"
            : null}
        </Typography>
        {/*  Quantity  */}
        <Grid container>
          <Grid className="d-flex" alignItems="center" item xs={12}>
            <IconButton
              color="primary"
              aria-label="upload picture"
              component="span"
            >
              <AddCircleIcon className="fs-2" onClick={increment} />
            </IconButton>
            <Box component="span" className="fs-2 mb-2" fontWeight="bold">
              {" "}
              {quantity}{" "}
            </Box>
            <IconButton
              color="primary"
              aria-label="upload picture"
              component="span"
            >
              <RemoveCircleIcon className="fs-2" onClick={decrement} />
            </IconButton>
          </Grid>
        </Grid>
        {/* Button Add To Cart */}
        <Button
          id="btn-detail"
          className="fw-bold"
          size="large"
          variant="outlined"
          startIcon={<AddShoppingCartIcon />}
          onClick={() => {
            addToCart(props.productDetail, quantity);
          }}
        >
          Add To Cart
        </Button>
      </Grid>
      {/* Snack Bar */}
      <SnackBar snackBar={snackBar} setSnackBar={setSnackBar}></SnackBar>
    </Grid>
  );
}

export default ProductInfo;
