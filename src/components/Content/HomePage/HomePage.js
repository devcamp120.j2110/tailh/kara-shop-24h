// Component
import LastestProductComponent from "./LastestProductComponent/LastestProductComponent"
import SliderComponent from "./SliderComponent/SliderComponent"
import ViewAllButtonComponent from "./ViewAllButtonComponent/ViewAllButtonComponent"

function HomePage(props) {
    return (
        <div>
            {/* Slider */}
            <SliderComponent selectedProduct={props.selectedProduct} getProductRelated={props.getProductRelated} productDataSlice={props.productData}></SliderComponent> 
            {/* Lastest Product */}
            <LastestProductComponent selectedProduct={props.selectedProduct} getProductRelated={props.getProductRelated} productData={props.productData}></LastestProductComponent>
            {/* View All Button */}
            <ViewAllButtonComponent></ViewAllButtonComponent>
        </div>
    )
}

export default HomePage
