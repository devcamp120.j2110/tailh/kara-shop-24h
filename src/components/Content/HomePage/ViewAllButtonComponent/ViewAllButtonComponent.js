// Material UI
import { Grid, Button } from "@mui/material";
// Router
import { useNavigate } from "react-router-dom";

function ViewAllButtonComponent() {
  const navigate = useNavigate();
  // Xử lý khi nhấn nút View ALL
  const goToProductPage = () => {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    navigate("/allproduct");
  };
  return (
    <Grid container className="mt-4" sx={{ justifyContent: "center"}} xs={12}>
      <Grid item sx={{ textAlign: "center" }} xs={4}>
        {/* View Button */}
        <Button size="large" variant="contained" onClick={goToProductPage}>View All</Button>
      </Grid>
    </Grid>
  );
}

export default ViewAllButtonComponent;
