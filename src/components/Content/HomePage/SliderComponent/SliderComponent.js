// Import Swiper
import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination, Navigation } from "swiper";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
// CSS
import "./SliderComponentStyle.css";
// Material UI
import { Grid, Box, Typography, Button } from "@mui/material";

function SliderComponent(props) {
  // Hàm xử lý khi nhấn nút view
  const viewDetailProduct = (product) => {
    // Select Product , Navigate đến trang Detail
    props.selectedProduct(product);
    // Lấy ra những sản phẩm liên quan
    props.getProductRelated(product.type);
  };

  return (
    <Grid
      justifyContent="center"
      sx={{ height: { xs: "200px", sm: "400px", md: "400px" } }}
      container
    >
      <Grid item xs={10} sm={10} md={10} lg={10} xl={10}>
        <Swiper
          slidesPerView={1}
          loop={true}
          pagination={{
            clickable: true,
          }}
          navigation={true}
          modules={[Pagination, Navigation]}
          className="mySwiper"
        >
          {props.productDataSlice !== ""
            ? props.productDataSlice.map((product, index) => {
                return (
                  <SwiperSlide key={index}>
                    <Grid container justifyContent="center">
                      <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Box
                          sx={{
                            height: { xs: "100px", sm: "300px", md: "300px" },
                            width: { xs: "160px", sm: "450px", md: "450px" },
                            margin: "0 auto",
                          }}
                        >
                          <img
                            className="w-100 h-100"
                            src={require(`../../../../assets/images/${product.imageUrl}`)}
                            alt={`Slide ${index}`}
                          />
                        </Box>
                      </Grid>
                      <Grid
                        item
                        xs={6}
                        sm={6}
                        md={6}
                        lg={6}
                        xl={6}
                        sx={{
                          display: { xs: "none", sm: "none", md: "block" },
                        }}
                      >
                        <Box
                          sx={{
                            width: "100%",
                            maxWidth: 500,
                            textAlign: "left",
                          }}
                        >
                          <Typography
                            sx={{ marginBottom: "8px", letterSpacing: "4px" }}
                            variant="h2"
                            gutterBottom
                            component="div"
                            className="text-capitalize fw-bold"
                          >
                            {product.name}
                          </Typography>
                          <Typography variant="body1" gutterBottom>
                            {product.description}
                          </Typography>
                          {/* View Button */}
                          <Button
                            sx={{
                              mt: "4px",
                              padding: "4px 24px",
                              fontSize: "18px",
                            }}
                            size="medium"
                            variant="outlined"
                            className="fw-bold slider-button"
                            onClick={() => {
                              viewDetailProduct(product);
                            }}
                          >
                            View
                          </Button>
                        </Box>
                      </Grid>
                    </Grid>
                  </SwiperSlide>
                );
              })
            : null}
        </Swiper>
      </Grid>
    </Grid>
  );
}

export default SliderComponent;
