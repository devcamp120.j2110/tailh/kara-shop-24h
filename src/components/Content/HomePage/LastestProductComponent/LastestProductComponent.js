// Material UI
import {
  Grid,
  Typography,
  Card,
  CardMedia,
  CardContent,
  CardActions,
  Button,
} from "@mui/material";
// Icon
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
// CSS
import "./LastestProductComponent.css";
// Redux
import { useDispatch } from "react-redux";
import { add } from "../../../../redux/Cart/CartSlice";
// Component
import SnackBar from "../../../SnackBar/SnackBarComponent";
// Hook
import { useState } from "react";

function LastestProductComponent(props) {
  // State
  const [snackBar, setSnackBar] = useState({
    display: false,
    type: "",
    text: "",
  });
  // Redux
  const dispatch = useDispatch();
  const addToCart = (product, quantity) => {
    // Gọi dispatch để update cart
    dispatch(add({ product: product, quantity: quantity }));
    setSnackBar({
      display: true,
      type: "success",
      text: "Product Added To Cart",
    });
  };
  // Hàm xử lý khi nhấn nút view
  const viewDetailProduct = (product) => {
    // Select Product , Navigate đến trang Detail
    props.selectedProduct(product);
    // Lấy ra những sản phẩm liên quan
    props.getProductRelated(product.type);
  };

  return (
    <Grid sx={{ justifyContent: "center", marginTop: "40px" }} container>
      {/* Title */}
      <Grid item xs={12} sm={12} md={12} lg={12} xl={12} className="my-2">
        <Typography
          align="center"
          variant="h2"
          className="fw-bold"
          component="div"
          gutterBottom
          sx={{ letterSpacing: "4px" }}
        >
          Lastest Product
        </Typography>
      </Grid>
      {/* Product List */}
      <Grid container spacing={4} xs={8} sm={8} md={8} lg={8} xl={8}>
        {props.productData !== ""
          ? props.productData.map((product, index) => {
              return (
                <Grid item xs={12} sm={12} md={12} lg={4} xl={4} key={index}>
                  <Card sx={{ maxWidth: "100%" }} className="p-4">
                    <CardMedia
                      component="img"
                      image={require(`../../../../assets/images/${product.imageUrl}`)}
                      alt="green iguana"
                      sx={{
                        objectFit: "contain",
                        height: { xs: "120px", sm: "200px" },
                      }}
                    />
                    <CardContent className="px-3 pt-3 pb-2">
                      <Typography
                        sx={{
                          fontWeight: "bold",
                          height: "44px",
                          overflow: "hidden",
                        }}
                        gutterBottom
                        variant="h4"
                        component="div"
                        className="cardHeader"
                      >
                        {product.name}
                      </Typography>
                      <Typography
                        className="text-description-wrap"
                        variant="body2"
                        color="text.secondary"
                      >
                        {product.description}
                      </Typography>
                      <Typography
                        className="m-0 d-inline text-decoration-line-through"
                        variant="h6"
                        gutterBottom
                        component="div"
                      >
                        {product.buyPrice}$
                      </Typography>
                      <Typography
                        className="m-0 fw-bold d-inline ms-1 price-text"
                        variant="h5"
                        gutterBottom
                        component="div"
                      >
                        {product.promotionPrice}$
                      </Typography>
                    </CardContent>

                    <CardActions
                      sx={{
                        display: "flex",
                        justifyContent: {
                          xs: "center",
                          sm: "center",
                          md: "flex-start",
                        },
                      }}
                    >
                      {/* View Button */}
                      <Button
                        className="fw-bold"
                        size="medium"
                        variant="outlined"
                        onClick={() => {
                          viewDetailProduct(product);
                        }}
                      >
                        View
                      </Button>
                      {/* Add Button */}
                      <Button
                        className="fw-bold"
                        size="medium"
                        variant="contained"
                        startIcon={<AddShoppingCartIcon />}
                        onClick={() => {
                          addToCart(product);
                        }}
                        sx={{
                          display: {
                            xs: "none",
                            sm: "none",
                            md: "inline-flex",
                          },
                        }}
                      >
                        Add To Cart
                      </Button>
                    </CardActions>
                  </Card>
                </Grid>
              );
            })
          : null}
      </Grid>
      <SnackBar snackBar={snackBar} setSnackBar={setSnackBar}></SnackBar>
    </Grid>
  );
}

export default LastestProductComponent;
