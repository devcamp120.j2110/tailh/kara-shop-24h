// Material UI
import { Grid, Typography, TextField, MenuItem, Button } from "@mui/material";
// MUI icon
import { Search, Clear } from "@mui/icons-material";
// Style
import { styled } from "@mui/material/styles";
import { orange } from "@mui/material/colors";
// CSS
import "./FilterComponentStyle.css";
// Hook
import { useState } from "react";

function FilterComponent(props) {
  // Render Select
  const type = [
    {
      value: "s",
      label: "S-Class",
    },
    {
      value: "a",
      label: "A-Class",
    },
    {
      value: "b",
      label: "B-Class",
    },
  ];
  const color = [
    {
      value: "red",
      label: "Red",
    },
    {
      value: "black",
      label: "Black",
    },
    {
      value: "white",
      label: "White",
    },
    {
      value: "blue",
      label: "Blue",
    },
  ];
  // Style
  const ColorButtonSearch = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(orange[400]),
    backgroundColor: orange[400],
    "&:hover": {
      backgroundColor: orange[500],
    },
  }));
  // State
  const [clearButton, setClearButton] = useState(false);
  //Handle onChange Input
  const handleChangeCarName = (event) => {
    props.setFilterObj({
      ...props.filterObj,
      name: event.target.value !== "" ? event.target.value.toLowerCase() : "",
    });
    event.target.value !== "" ? setClearButton(true) : setClearButton(false);
  };
  const handleChangeType = (event) => {
    props.setFilterObj({ ...props.filterObj, type: event.target.value });
    event.target.value !== "" ? setClearButton(true) : setClearButton(false);
  };
  const handleChangeColor = (event) => {
    props.setFilterObj({ ...props.filterObj, color: event.target.value });
    event.target.value !== "" ? setClearButton(true) : setClearButton(false);
  };
  // Min Price và Max Price chỉ cho nhập số
  const handleChangeMinPrice = (event) => {
    props.setFilterObj({
      ...props.filterObj,
      minPrice: event.target.value.replace(/\D/, ""),
    });
    event.target.value !== "" ? setClearButton(true) : setClearButton(false);
  };
  const handleChangeMaxPrice = (event) => {
    props.setFilterObj({
      ...props.filterObj,
      maxPrice: event.target.value.replace(/\D/, ""),
    });
    event.target.value !== "" ? setClearButton(true) : setClearButton(false);
  };
  // Hàm xử lý khi nhấn nút search
  const onButtonSearchClick = () => {
    if (
      props.filterObj.name.trim() +
        props.filterObj.type.trim() +
        props.filterObj.color.trim() +
        props.filterObj.minPrice.trim() +
        props.filterObj.maxPrice.trim() !==
      ""
    ) {
      // Call Api Khi input có giá trị
      props.handleFilterProduct(
        props.limit,
        (props.currentPage - 1) * props.limit,
        props.filterObj
      );
    } else {
      // TH input rỗng reload bảng
      props.getProductList(props.limit, (props.currentPage - 1) * props.limit);
    }
  };
  // Hàm xử lý khi nhấn nút clear
  const onClearButtonClick = () => {
    // Clear Input
    props.setFilterObj({
      name: "",
      type: "",
      color: "",
      minPrice: "",
      maxPrice: "",
    });
    // Reload
    props.getProductList(props.limit, (props.currentPage - 1) * props.limit);
  };
  return (
    <Grid
      container
      className="p-5"
      style={{ backgroundColor: "aliceblue", borderRadius: "20px" }}
    >
      {/* Category Header */}
      <Grid item xs={12}>
        <Typography
          align="center"
          fontWeight="bold"
          variant="h5"
          gutterBottom
          component="div"
        >
          Category
        </Typography>
      </Grid>
      {/* Search Car Name */}
      <Grid item xs={12}>
        <Typography
          align="left"
          fontWeight="bold"
          variant="h6"
          gutterBottom
          component="div"
          className="m-0"
        >
          Name
        </Typography>
        <TextField
          color="warning"
          sx={{ my: 1 }}
          fullWidth
          id="outlined-search"
          label="Car Name"
          size="medium"
          type="search"
          className="input-background"
          value={props.filterObj.name}
          onChange={handleChangeCarName}
        />
      </Grid>
      {/* Search Car Type */}
      <Grid item xs={12}>
        <Typography
          align="left"
          fontWeight="bold"
          variant="h6"
          gutterBottom
          component="div"
          className="m-0"
        >
          Type
        </Typography>
        <TextField
          color="warning"
          sx={{ my: 1 }}
          fullWidth
          id="outlined-search"
          label="Car Type"
          size="medium"
          select
          value={props.filterObj.type}
          onChange={handleChangeType}
          className="input-background"
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {type.map((type, index) => {
            return (
              <MenuItem key={index} value={type.value}>
                {type.label}
              </MenuItem>
            );
          })}
        </TextField>
      </Grid>
      {/* Color */}
      <Grid item xs={12}>
        <Typography
          align="left"
          fontWeight="bold"
          variant="h6"
          gutterBottom
          component="div"
          className="m-0"
        >
          Color
        </Typography>
        <TextField
          color="warning"
          sx={{ my: 1 }}
          fullWidth
          id="outlined-search"
          label="Car Color"
          size="medium"
          select
          value={props.filterObj.color}
          onChange={handleChangeColor}
          className="input-background"
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {color.map((color, index) => {
            return (
              <MenuItem key={index} value={color.value}>
                {color.label}
              </MenuItem>
            );
          })}
        </TextField>
      </Grid>
      {/* Search Min-Max Price */}
      <Grid item xs={12}>
        <Typography
          align="left"
          fontWeight="bold"
          variant="h6"
          gutterBottom
          component="div"
          className="m-0"
        >
          Price
        </Typography>
        <Grid container>
          <Grid item xs={5}>
            <TextField
              color="warning"
              sx={{ my: 1 }}
              fullWidth
              id="outlined-search"
              label="Min Price"
              type="search"
              size="small"
              className="input-background"
              value={props.filterObj.minPrice}
              onChange={handleChangeMinPrice}
            />
          </Grid>
          <Grid item xs={2} style={{ margin: "auto", textAlign: "center" }}>
            -
          </Grid>
          <Grid item xs={5}>
            <TextField
              color="warning"
              sx={{ my: 1 }}
              fullWidth
              id="outlined-search"
              label="Max Price"
              type="search"
              size="small"
              className="input-background"
              value={props.filterObj.maxPrice}
              onChange={handleChangeMaxPrice}
            />
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={6}>
        {/* Search Button */}
        <ColorButtonSearch
          sx={{ fontWeight: "bold", mt: "1rem" }}
          variant="contained"
          size="large"
          startIcon={<Search />}
          onClick={onButtonSearchClick}
        >
          Search
        </ColorButtonSearch>
      </Grid>
      <Grid className="d-flex justify-content-center" item xs={6}>
        {/* Clear Button */}
        {clearButton ? (
          <Button
            sx={{ fontWeight: "bold", mt: "1rem" }}
            variant="outlined"
            size="large"
            color="error"
            startIcon={<Clear />}
            onClick={onClearButtonClick}
          >
            Clear
          </Button>
        ) : null}
      </Grid>
    </Grid>
  );
}

export default FilterComponent;
