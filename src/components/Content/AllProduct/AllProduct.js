// Material UI
import { Grid } from "@mui/material";
// Component
import BreadcrumbComponent from "../../Breadcrumb/BreadcrumbComponent";
import FilterComponent from "./FilterComponent/FilterComponent";
import NavigationComponent from "./NavigationComponent/NavigationComponent";
import ProductListComponent from "./ProductListComponent/ProductListComponent";

function AllProduct(props) {
  return (
    <Grid container justifyContent="center">
      {/* Breadcrumb */}
      <BreadcrumbComponent></BreadcrumbComponent>
      <Grid className="mt-4" container>
        {/* Filter */}
        <Grid item xs={3} className="pt-5">
          <FilterComponent
            handleFilterProduct={props.handleFilterProduct}
            filterObj={props.filterObj}
            setFilterObj={props.setFilterObj}
            currentPage={props.currentPage}
            limit={props.limit}
            getProductList={props.getProductList}
          ></FilterComponent>
        </Grid>
        {/* Product List */}
        <Grid item xs={9}>
          <ProductListComponent
            productList={props.productList}
            selectedProduct={props.selectedProduct}
            getProductRelated={props.getProductRelated}
          ></ProductListComponent>
        </Grid>
      </Grid>
      <Grid justifyContent="end" container>
        <Grid className="d-flex justify-content-center mt-4" item xs={9}>
          <NavigationComponent
            numberOfPage={props.numberOfPage}
            currentPage={props.currentPage}
            setCurrentPage={props.setCurrentPage}
          ></NavigationComponent>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default AllProduct;
