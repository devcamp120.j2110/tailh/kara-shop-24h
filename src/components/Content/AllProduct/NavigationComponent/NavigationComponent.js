// Material UI
import { Pagination, Stack } from "@mui/material";

function NavigationComponent(props) {
    // Xử lý sự kiện khi đổi trang
  const changePageHandle = (event, value) => {
    props.setCurrentPage(value);
    window.scrollTo({ top: 0, behavior: "smooth" });
  };
  return (
    <Stack spacing={2}>
      <Pagination
        onChange={changePageHandle}
        count={props.numberOfPage}
        defaultPage={props.currentPage}
        color="primary"
      />
    </Stack>
  );
}

export default NavigationComponent;
