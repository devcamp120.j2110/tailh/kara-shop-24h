// Material UI
import {
  Grid,
  Typography,
  Card,
  CardMedia,
  CardContent,
  CardActions,
  Button,
} from "@mui/material";
// Icon
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import StarBorderPurple500Icon from "@mui/icons-material/StarBorderPurple500";
// IMG
import NotFoundImg from "../../../../assets/images/not-found.png";
// Hook
import { useState } from "react";
// Redux
import { useDispatch } from "react-redux";
import { add } from "../../../../redux/Cart/CartSlice";
// Component
import SnackBar from "../../../SnackBar/SnackBarComponent";

function ProductListComponent(props) {
  // State
  const [snackBar, setSnackBar] = useState({
    display: false,
    type: "",
    text: "",
  });

  // Xử lý khi nhấn xem chi tiết sản phẩm
  const viewDetailProduct = (product) => {
    // Navigate đến trang detail
    props.selectedProduct(product);
    // Lọc ra những sản phẩm liên quan
    props.getProductRelated(product.type);
  };

  const dispatch = useDispatch();
  // Hàm thêm giỏ hàng
  // input product + quantity
  const addToCart = (product, quantity) => {
    // Gọi dispatch để update cart
    dispatch(add({ product: product, quantity: quantity }));
    // Hiện Snackbar Success
    setSnackBar({
      display: true,
      type: "success",
      text: "Product Added To Cart",
    });
  };

  return (
    <Grid container justifyContent="center">
      {/* Product List Header */}
      <Grid item xs={12}>
        <Typography
          fontWeight="bold"
          align="center"
          variant="h2"
          gutterBottom
          component="div"
        >
          <StarBorderPurple500Icon />
          <StarBorderPurple500Icon />
          <StarBorderPurple500Icon />
          All Product
          <StarBorderPurple500Icon />
          <StarBorderPurple500Icon />
          <StarBorderPurple500Icon />
        </Typography>
      </Grid>
      {/* Product List Item */}
      <Grid item xs={10}>
        <Grid container spacing={4}>
          {props.productList !== ""
            ? props.productList.map((product, index) => {
                return (
                  <Grid item xs={4} key={index}>
                    <Card sx={{ maxWidth: "100%" }} className="p-4">
                      <CardMedia
                        component="img"
                        image={require(`../../../../assets/images/${product.imageUrl}`)}
                        alt="green iguana"
                        style={{ objectFit: "contain", height: "220px" }}
                      />
                      <CardContent className="px-3 pt-3 pb-2">
                        <Typography
                          sx={{
                            fontWeight: "bold",
                            height: "44px",
                            overflow: "hidden",
                          }}
                          gutterBottom
                          variant="h4"
                          component="div"
                          className="cardHeader"
                        >
                          {product.name}
                        </Typography>
                        <Typography
                          sx={{
                            height: "80px",
                            overflow: "hidden",
                          }}
                          variant="body2"
                          color="text.secondary"
                        >
                          {product.description}
                        </Typography>
                        <Typography
                          className="m-0 d-inline text-decoration-line-through"
                          variant="h6"
                          gutterBottom
                          component="div"
                        >
                          {product.buyPrice}$
                        </Typography>
                        <Typography
                          className="m-0 fw-bold d-inline ms-1 price-text"
                          variant="h5"
                          gutterBottom
                          component="div"
                        >
                          {product.promotionPrice}$
                        </Typography>
                      </CardContent>
                      <CardActions>
                        {/* View Button */}
                        <Button
                          className="fw-bold"
                          size="medium"
                          variant="outlined"
                          onClick={() => {
                            viewDetailProduct(product);
                          }}
                        >
                          View
                        </Button>
                        {/* Add Button */}
                        <Button
                          className="fw-bold"
                          size="medium"
                          variant="contained"
                          startIcon={<AddShoppingCartIcon />}
                          onClick={() => {
                            addToCart(product);
                          }}
                        >
                          Add To Cart
                        </Button>
                      </CardActions>
                    </Card>
                  </Grid>
                );
              })
            : null}
            {/* TH lọc sản phẩm mà k tìm thấy sản phẩm */}
          {props.productList.length === 0 ? (
            <Grid item align="center" xs={12}>
              <img
                className="my-5 mx-auto p-4"
                src={NotFoundImg}
                alt="Not Found"
              />
              <Typography
                fontWeight="bold"
                align="center"
                variant="h4"
                gutterBottom
                component="div"
                style={{ color: "rgba(51, 51, 51,0.5)" }}
              >
                No Products Found
              </Typography>
            </Grid>
          ) : null}
        </Grid>
      </Grid>
      {/* Snack Bar */}
      <SnackBar snackBar={snackBar} setSnackBar={setSnackBar}></SnackBar>
    </Grid>
  );
}

export default ProductListComponent;
