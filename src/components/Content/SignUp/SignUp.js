// Material UI
import {
  Grid,
  Typography,
  TextField,
  Input,
  InputAdornment,
  IconButton,
  Button,
} from "@mui/material";
// Icon
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
// Hook
import { useState } from "react";
// Fetch
import fetchApi from "../../../fetchApi";
// Router
import { useNavigate } from "react-router-dom";
// Component
import SnackBar from "../../SnackBar/SnackBarComponent";

function SignUp() {
  //  Navigate
  const navigate = useNavigate();
  const goToAccountCreatedPage = () => {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    navigate("/account-created");
  };
  // State
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState({
    showPassword: false,
  });
  const [passwordConfirm, setPasswordConfirm] = useState({
    showPasswordConfirm: false,
  });
  const [fullname, setFullname] = useState("");
  const [address, setAddress] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [city, setCity] = useState("");
  const [country, setCountry] = useState("");
  const [snackBar, setSnackBar] = useState({
    display: false,
    type: "",
    text: "",
  });
  // Handle
  // Input Email
  const handleChangeEmail = (e) => {
    setEmail(e.target.value);
  };
  // Input Password
  const handleChangePassword = (prop) => (event) => {
    setPassword({ ...password, [prop]: event.target.value });
  };
  const handleClickShowPassword = () => {
    setPassword({
      ...password,
      showPassword: !password.showPassword,
    });
  };
  // Input Password Confirm
  const handleChangePasswordConfirm = (prop) => (event) => {
    setPasswordConfirm({ ...passwordConfirm, [prop]: event.target.value });
  };
  const handleClickShowPasswordConfirm = () => {
    setPasswordConfirm({
      ...passwordConfirm,
      showPasswordConfirm: !passwordConfirm.showPasswordConfirm,
    });
  };
  // Input Fullname
  const handleChangeFullname = (e) => {
    setFullname(e.target.value);
  };
  // Input Address
  const handleChangeAddress = (e) => {
    setAddress(e.target.value);
  };
  // Input Address
  const handleChangePhoneNumber = (e) => {
    setPhoneNumber(e.target.value);
  };
  // Input City
  const handleChangeCity = (e) => {
    setCity(e.target.value);
  };
  // Input Country
  const handleChangeCountry = (e) => {
    setCountry(e.target.value);
  };

  // Create Customer
  const createCustomer = () => {
    // Validate Data
    var isValidFormRegister = validateCustomer();
    if (isValidFormRegister) {
      // Check Email Exists
      checkEmailExists(email); //callApiCreateCustomer nếu không tìm thấy customer trên database
    }
  };
  // Validate
  const validateCustomer = () => {
    // Check Email
    if (email.trim() === "") {
      setSnackBar({
        display: true,
        type: "error",
        text: "Invalid Email.",
      });
      return false;
    }
    //Check Email theo Form
    var vEmailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
    if (!email.match(vEmailRegex)) {
      setSnackBar({
        display: true,
        type: "error",
        text: "Invalid Email.",
      });
      return false;
    }
    // Check Password
    //7 to 15 characters which contain only characters, numeric digits, underscore and first character must be a letter
    //7 đến 15 ký tự chỉ bao gồm chữ ,số ,gạch chân và kí tự đầu tiên phải là 1 chữ
    var vPassRegex = /^[A-Za-z]\w{7,14}$/;
    if (!password.value.match(vPassRegex)) {
      setSnackBar({
        display: true,
        type: "error",
        text: "Invalid Password.",
      });
      return false;
    }
    if (password.value !== passwordConfirm.value) {
      setSnackBar({
        display: true,
        type: "error",
        text: "Password and Confirm Password must be match.",
      });
      return false;
    }
    // Check Fullname
    if (fullname.trim() === "") {
      setSnackBar({
        display: true,
        type: "error",
        text: "Invalid Fullname.",
      });
      return false;
    }
    // Check Address
    if (address.trim() === "") {
      setSnackBar({
        display: true,
        type: "error",
        text: "Invalid Address.",
      });
      return false;
    }
    // Check Address
    if (phoneNumber.trim() === "") {
      setSnackBar({
        display: true,
        type: "error",
        text: "Invalid Phone Number.",
      });
      return false;
    }
    if (isNaN(phoneNumber.trim())) {
      setSnackBar({
        display: true,
        type: "error",
        text: "Invalid Phone Number.",
      });
      return false;
    }
    // Check City
    if (city.trim() === "") {
      setSnackBar({
        display: true,
        type: "error",
        text: "Invalid City.",
      });
      return false;
    }
    // Check Country
    if (country.trim() === "") {
      setSnackBar({
        display: true,
        type: "error",
        text: "Invalid Country.",
      });
      return false;
    }
    return true;
  };
  // Check Email Exists
  const checkEmailExists = (paramEmail) => {
    fetchApi("http://localhost:8080/customers/email/" + paramEmail)
      .then((data) => {
        console.log(data);
        // TH không tim thấy customer thì call API create customer
        if (data.customer === null) {
          callApiCreateCustomer();
        }else {
          // Toast Error
          setSnackBar({
            display: true,
            type: "error",
            text: "Email address is already exists.",
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Call Api Create Customers
  const callApiCreateCustomer = () => {
    // Tạo Obj request
    const customerRequest = {
      fullname: fullname,
      phoneNumber: phoneNumber,
      email: email,
      password: password.value,
      address: address,
      city: city,
      country: country,
    };
    const customer = {
      method: "POST",
      body: JSON.stringify(customerRequest),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    };
    fetchApi("http://localhost:8080/customers", customer)
      .then((data) => {
        console.log(data);
        //reset all state
        resetState();
        // Navigate Đến Trang Account Created
        goToAccountCreatedPage();
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Hàm reset State
  const resetState = () => {
    setEmail("");
    setPassword({
      showPassword: false,
    });
    setPasswordConfirm({
      showPasswordConfirm: false,
    });
    setFullname("");
    setAddress("");
    setPhoneNumber("");
    setCity("");
    setCountry("");
  };
  return (
    <Grid justifyContent="center" container>
      {/* Header */}
      <Grid item xs={12}>
        <Typography
          align="center"
          variant="h2"
          component="div"
          fontWeight="bold"
          className="mb-2"
        >
          Register
        </Typography>
        <Typography
          align="center"
          variant="subtitle1"
          gutterBottom
          component="div"
          fontWeight="bold"
        >
          Create your account. It's free and only takes a minute.
        </Typography>
      </Grid>
      {/* Form Register */}
      <Grid item xs={8}>
        {/* Email Input */}
        <Grid container className="mt-3">
          <Grid className="w-100 my-1" item xs={2}>
            <Typography variant="h6" component="div" fontWeight="bold">
              Email:
            </Typography>
          </Grid>
          <Grid item xs={10}>
            <TextField
              className="w-100 my-1"
              variant="standard"
              type="email"
              placeholder="email@address.com"
              onChange={handleChangeEmail}
            />
          </Grid>
        </Grid>
        {/* Password Input */}
        <Grid container className="mt-3">
          <Grid className="w-100 my-1" item xs={2}>
            <Typography variant="h6" component="div" fontWeight="bold">
              Password :
            </Typography>
          </Grid>
          <Grid item xs={10}>
            <Input
              className="w-100 my-1"
              type={password.showPassword ? "text" : "password"}
              value={password.value}
              onChange={handleChangePassword("value")}
              placeholder="7 to 15 characters"
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                  >
                    {password.showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
            />
          </Grid>
        </Grid>
        {/* Confirm Password Input */}
        <Grid container className="mt-3">
          <Grid className="w-100 my-1" item xs={2}>
            <Typography variant="h6" component="div" fontWeight="bold">
              Confirm Password :
            </Typography>
          </Grid>
          <Grid item xs={10}>
            <Input
              className="w-100 my-1"
              type={passwordConfirm.showPasswordConfirm ? "text" : "password"}
              value={passwordConfirm.value}
              onChange={handleChangePasswordConfirm("value")}
              placeholder="7 to 15 characters"
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPasswordConfirm}
                  >
                    {passwordConfirm.showPasswordConfirm ? (
                      <VisibilityOff />
                    ) : (
                      <Visibility />
                    )}
                  </IconButton>
                </InputAdornment>
              }
            />
          </Grid>
        </Grid>
        {/* Fullname */}
        <Grid container className="mt-3">
          <Grid className="w-100 my-1" item xs={2}>
            <Typography variant="h6" component="div" fontWeight="bold">
              Fullname:
            </Typography>
          </Grid>
          <Grid item xs={10}>
            <TextField
              className="w-100 my-1"
              variant="standard"
              type="email"
              placeholder="Enter your name here"
              onChange={handleChangeFullname}
            />
          </Grid>
        </Grid>
        {/* Address */}
        <Grid container className="mt-3">
          <Grid className="w-100 my-1" item xs={2}>
            <Typography variant="h6" component="div" fontWeight="bold">
              Address:
            </Typography>
          </Grid>
          <Grid item xs={10}>
            <TextField
              className="w-100 my-1"
              variant="standard"
              type="email"
              placeholder="Enter your address here"
              onChange={handleChangeAddress}
            />
          </Grid>
        </Grid>
        {/* Phone Number */}
        <Grid container className="mt-3">
          <Grid className="w-100 my-1" item xs={2}>
            <Typography variant="h6" component="div" fontWeight="bold">
              Phone Number:
            </Typography>
          </Grid>
          <Grid item xs={10}>
            <TextField
              className="w-100 my-1"
              variant="standard"
              type="email"
              placeholder="Enter your Phone Number here"
              onChange={handleChangePhoneNumber}
            />
          </Grid>
        </Grid>
        {/* City */}
        <Grid container className="mt-3">
          <Grid className="w-100 my-1" item xs={2}>
            <Typography variant="h6" component="div" fontWeight="bold">
              City:
            </Typography>
          </Grid>
          <Grid item xs={10}>
            <TextField
              className="w-100 my-1"
              variant="standard"
              type="email"
              placeholder="Enter your city here"
              onChange={handleChangeCity}
            />
          </Grid>
        </Grid>
        {/* Country */}
        <Grid container className="mt-3">
          <Grid className="w-100 my-1" item xs={2}>
            <Typography variant="h6" component="div" fontWeight="bold">
              Country:
            </Typography>
          </Grid>
          <Grid item xs={10}>
            <TextField
              className="w-100 my-1"
              variant="standard"
              type="email"
              placeholder="Enter your country here"
              onChange={handleChangeCountry}
            />
          </Grid>
        </Grid>
        {/* Button */}
        <Grid container className="mt-3">
          <Grid align="right" item xs={12}>
            <Button
              className="fw-bold"
              size="large"
              variant="contained"
              onClick={createCustomer}
            >
              Create Account
            </Button>
          </Grid>
        </Grid>
      </Grid>
      {/* Snack Bar */}
      <SnackBar snackBar={snackBar} setSnackBar={setSnackBar}></SnackBar>
    </Grid>
  );
}

export default SignUp;
