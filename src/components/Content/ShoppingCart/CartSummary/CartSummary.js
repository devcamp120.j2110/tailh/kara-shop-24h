// Component
import UserInfoModal from "../../../UserInfoModal/UserInfoModal";
import SnackBar from "../../../SnackBar/SnackBarComponent";
// Material UI
import { Grid, Typography, Button } from "@mui/material";
// Hook
import React from "react";
import { useState } from "react";
// Style
import { red , orange} from "@mui/material/colors";
import { styled } from "@mui/material/styles";
// Redux
import { useSelector } from "react-redux";
// Icon
import DoneOutlineIcon from "@mui/icons-material/DoneOutline";
// Fetch 
import fetchApi from "../../../../fetchApi";

function CartSummary() {
  // State
  const [snackBar, setSnackBar] = useState({
    display:false,
    type:"warning",
    text:""
  });
  const [userInfoModal,setUserInfoModal] = useState(false);
  // Redux
  const cartItems = useSelector((state) => state.cart.cartItems);
  const user = useSelector((state) => state.user.user);
  // Style
  const priceColor = red[600];
  // Style Button
  const ColorButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(orange[400]),
    backgroundColor: orange[400],
    "&:hover": {
      backgroundColor: orange[500],
    },
  }));
  // Handle
  // Hàm callAPI tạo order
  const callApiCreateOrder = (userID,orderDetails) => {
    const orderRequest = {
      customer: userID,
      orderDetails:orderDetails
    };
    const order = {
      method: "POST",
      body: JSON.stringify(orderRequest),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    };
    fetchApi("http://localhost:8080/orders", order)
      .then((data) => {
        console.log(data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Hàm hiển thị modal confirm user info 
  const openModalConfirmInfoUser = () => {
    if (user !== null) {
      setUserInfoModal(true);
    } else {
      setSnackBar({
        display:true,
        type:"warning",
        text:"You Must Be Logged To Checkout"
      })
    }
  };
  return (
    <>
      <Typography variant="h5" fontWeight="bold" gutterBottom component="div">
        Payment Summary
      </Typography>
      <Grid container className="p-4" style={{ backgroundColor: "aliceblue" }}>
        <Grid
          container
          className="py-2 my-2"
          style={{ borderBottom: `8px dotted #333` }}
        >
          {cartItems.map((cartItem, index) => {
            return (
              <React.Fragment key={index}>
                <Grid item xs={8}>
                  <Typography
                    color="#333"
                    fontWeight="bold"
                    variant="h6"
                    component="div"
                  >
                    {cartItem.product.name.toUpperCase()} x {cartItem.quantity}
                  </Typography>
                </Grid>
                <Grid item xs={4}>
                  {/* Price Each */}
                  <Typography
                    color={priceColor}
                    fontWeight="bold"
                    align="right"
                    variant="h5"
                    gutterBottom
                    component="div"
                  >
                    ${cartItem.product.promotionPrice * cartItem.quantity}
                  </Typography>
                </Grid>
              </React.Fragment>
            );
          })}
        </Grid>
        <Grid item xs={8}>
          <Typography fontWeight="bold" variant="h5" component="div">
            Total Amount
          </Typography>
        </Grid>
        {/* Total Price */}
        <Grid item xs={4}>
          <Typography
            color={priceColor}
            fontWeight="bold"
            align="right"
            variant="h5"
            gutterBottom
            component="div"
          >
            $
            {cartItems.reduce((total, current) => {
              return total + current.product.promotionPrice * current.quantity;
            }, 0)}
          </Typography>
        </Grid>
        <Grid item xs={12}>
          {/* Button Checkout */}
          <ColorButton
            size="large"
            variant="contained"
            endIcon={<DoneOutlineIcon />}
            className="float-end fw-bold"
            onClick={() => {
              openModalConfirmInfoUser();
            }}
          >
            Check Out
          </ColorButton>
        </Grid>
      </Grid>
      {/* Snack Bar */}
      <SnackBar
        snackBar={snackBar}
        setSnackBar={setSnackBar}
      ></SnackBar>
      {/* User Info */}
      <UserInfoModal setSnackBar={setSnackBar} userInfoModal={userInfoModal} setUserInfoModal={setUserInfoModal} callApiCreateOrder={callApiCreateOrder}></UserInfoModal>
    </>
  );
}

export default CartSummary;
