// Material UI
import { Grid, Typography } from "@mui/material";
// Component
import CartOrder from "../ShoppingCart/CartOrder/CartOrder";
import CartSummary from "./CartSummary/CartSummary";
// Redux
import { useSelector } from "react-redux";
// Style
import {  grey } from "@mui/material/colors";
import React from "react";

function ShoppingCart() {
  // Dùng Selecter để lấy cartItems
  const cartItems = useSelector((state) => state.cart.cartItems);
  // Style
  const greyColor = grey[600];
  return (
    <Grid container justifyContent="center" xs={10}>
      {cartItems.length === 0 ? (
        <>
          <Grid align="center" item xs={12}>
            <img src={require("../../../assets/images/cart-empty.png")} />
          </Grid>
          <Grid item xs={12}>
            <Typography
              variant="h4"
              fontWeight="bold"
              gutterBottom
              component="div"
              align="center"
              color={greyColor}
              my={3}
            >
              Your Cart Is Empty
            </Typography>
          </Grid>
        </>
      ) : (
        <React.Fragment>
          <Grid item xs={8}>
            {/* Order */}
            <CartOrder></CartOrder>
          </Grid>
          <Grid item xs={4}>
            {/* Summary */}
            <CartSummary></CartSummary>
          </Grid>{" "}
        </React.Fragment>
      )}
    </Grid>
  );
}

export default ShoppingCart;
