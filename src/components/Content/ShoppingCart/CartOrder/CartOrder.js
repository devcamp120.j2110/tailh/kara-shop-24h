// Material UI
import { Grid, Typography, List, ListItem, IconButton } from "@mui/material";
// Style Color
import { red, grey } from "@mui/material/colors";
// Icon
import RemoveCircleOutlineOutlinedIcon from "@mui/icons-material/RemoveCircleOutlineOutlined";
import AddCircleOutlineOutlinedIcon from "@mui/icons-material/AddCircleOutlineOutlined";
import ClearIcon from "@mui/icons-material/Clear";
// Redux
import { useSelector, useDispatch } from "react-redux";
import { add, remove, deleteCartItem } from "../../../../redux/Cart/CartSlice";
function CartOrder() {
  // Style Color
  const priceColor = red[600];
  const quantityColor = grey[500];
  const dividerColor = grey[300];
  // Redux
  const cartItems = useSelector((state) => state.cart.cartItems);
  const dispatch = useDispatch();
  // Hàm xử lý khi nhấn Icon add
  const addToCart = (product, quantity) => {
    dispatch(add({ product: product, quantity: quantity }));
  };
  // Hàm xử lý khi nhấn Icon remove
  const removeToCart = (product, quantity) => {
    dispatch(remove({ product: product, quantity: quantity }));
  };
  // Hàm xử lý khi nhấn nút xoá
  const deleteItem = (index) => {
    dispatch(deleteCartItem(index));
  };
  return (
    <>
      <Typography variant="h4" fontWeight="bold" gutterBottom component="div">
        Order
      </Typography>
      {cartItems.map((cartItem, index) => {
        return (
          <Grid
            key={index}
            container
            className="mx-2 py-2"
            style={{ borderBottom: `1px solid ${dividerColor}` }}
          >
            <Grid item xs={2}>
              <img
                height="100%"
                width="100%"
                src={require(`../../../../../src/assets/images/${cartItem.product.imageUrl}`)}
                alt="cart-img"
              />
            </Grid>
            <Grid item xs={4} className="ps-4">
              <List>
                <ListItem disablePadding>
                  <Typography variant="h5" fontWeight="bold" component="div">
                    {cartItem.product.name.toUpperCase()}
                  </Typography>
                </ListItem>
                <ListItem disablePadding>
                  <Typography variant="h6" component="div">
                    Color: {cartItem.product.color.toUpperCase()}
                  </Typography>
                </ListItem>
                <ListItem disablePadding>
                  <Typography variant="h6" component="div">
                    Type: {cartItem.product.type.toUpperCase()}-Class
                  </Typography>
                </ListItem>
              </List>
            </Grid>
            <Grid item xs={2} color={priceColor} className="m-auto">
              <Typography
                align="center"
                fontWeight="bold"
                variant="h5"
                component="div"
              >
                ${cartItem.product.promotionPrice}
              </Typography>
            </Grid>
            <Grid item xs={2} className="m-auto">
              <Typography
                align="center"
                fontWeight="bold"
                variant="h5"
                component="div"
              >
                <span style={{ color: quantityColor, fontSize: "16px" }}>
                  Quantity:
                </span>{" "}
                {/* ADD ICON */}
                <AddCircleOutlineOutlinedIcon
                  role="button"
                  onClick={() => {
                    addToCart(cartItem.product);
                  }}
                />{" "}
                {cartItem.quantity}{" "}
                {/* REMOVE ICON */}
                <RemoveCircleOutlineOutlinedIcon
                  role="button"
                  onClick={() => {
                    removeToCart(cartItem.product);
                  }}
                />
              </Typography>
            </Grid>
            <Grid item xs={2} align="center" className="m-auto">
              {/* CLEAR BUTTON */}
              <IconButton
                onClick={() => {
                  deleteItem(index);
                }}
                className="fw-bold"
                color="error"
                aria-label="delete"
              >
                <ClearIcon className="fs-2" />
              </IconButton>
            </Grid>
          </Grid>
        );
      })}
    </>
  );
}

export default CartOrder;
