// Logo Img
import Logo from "../../../assets/Logo/logo_transparent.png" 
// Router
import { useNavigate } from "react-router-dom";

function HeaderLogoComponent() {
  // Navigate Page
  const navigate = useNavigate();
  const goToHomePage = () => {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    navigate("/");
  };
  return (<img onClick={goToHomePage} src={Logo} alt="logo" style={{height: "100%",width:"80px" , cursor: "pointer"}}></img>);
}

export default HeaderLogoComponent;

