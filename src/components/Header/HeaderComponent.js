// Component
import HeaderLogoComponent from "./HeaderLogo/HeaderLogoComponent";
import LoginButtonComponent from "./LoginButton/LoginButtonComponent";
// Fetch Api
import fetchApi from "../../fetchApi";
// Material UI
import Grid from "@mui/material/Grid";
// CSS
import "./HeaderComponentStyle.css";
// Hook
import { useEffect } from "react";
// Fire Base
import { auth, googleProvider } from "../../firebase";
// Redux
import { useDispatch } from "react-redux";
import { update } from "../../redux/User/UserSlice";

function HeaderComponent() {
  // Redux
  const dispatch = useDispatch();
  const updateUser = (data) => {
    dispatch(update(data));
  };
  // Hàm Tạo Customer Khi Đăng Nhập Bằng Fire Base
  const createCustomer = (data) => {
    // Tạo Object Request
    const customerRequest = {
      fullname: data.displayName,
      phoneNumber: data.phoneNumber,
      email: data.email,
      photoURL: data.photoURL,
      password: data.password ? data.password : "",
      address: data.address ? data.address : "",
      city: data.city ? data.city : "",
      country: data.country ? data.country : "",
    };
    // Tạo Header +  Boby + Method
    const customer = {
      method: "POST",
      body: JSON.stringify(customerRequest),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    };
    // Call API create customer or Đăng nhập
    // Trường hợp đã đăng nhập bằng fire base trước đó thì getOneCustomerByEmail để đăng nhập
    fetchApi("http://localhost:8080/customers", customer)
      .then((data) => {
        console.log(data);
        return data.email;
      })
      .then((email) => {
        console.log(email);
        getOneCustomerByEmail(email);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  // Call API lấy dữ liệu ng dùng từ DB bằng email
  const getOneCustomerByEmail = (email) => {
    fetchApi("http://localhost:8080/customers/email/" + email)
      .then((data) => {
        console.log(data);
        updateUser(data.customer); //Lưu vào Redux
      })
      .catch((err) => {
        console.log(err);
      });
  };

  // Hàm logout
  const logout = () => {
    auth
      .signOut()
      .then(() => {
        updateUser(null);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // hàm login gg
  const loginGoogle = () => {
    auth
      .signInWithPopup(googleProvider)
      .then((result) => {
        createCustomer(result.user); //NodeJS session
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // hàm useEffect đc gọi khi reload trang
  // Giữ cho accout vẫn trong trang thái đang nhập
  useEffect(() => {
    auth.onAuthStateChanged((result) => {
      const userReload = {
        fullname: result.displayName,
        phoneNumber: result.phoneNumber,
        email: result.email,
        photoURL: result.photoURL,
        password: result.password ? result.password : "",
        address: result.address ? result.address : "",
        city: result.city ? result.city : "",
        country: result.country ? result.country : "",
      };
      updateUser(userReload);
    });
  }, []);

  // Hàm đổi background khi onScroll Navbar
  const changeBackground = () => {
    var navbar = document.querySelector("#navbar");
    if (window.scrollY > 0) {
      navbar.classList.add("active");
    }
    if (window.scrollY === 0) {
      navbar.classList.remove("active");
    }
  };
  window.addEventListener("scroll", changeBackground);

  return (
    <Grid
      id="navbar"
      className="fixed-top navbar"
      container
      alignItems="center"
      justifyContent="center"
    >
      <Grid item xs={10} sm={10} md={10} lg={10} xl={10}>
        <Grid container justifyContent="center" alignItems="center">
          {/* Logo */}
          <Grid item xs={2} sm={2} md={2} lg={2} xl={2}>
            <HeaderLogoComponent></HeaderLogoComponent>
          </Grid>
          {/* Button Loggin */}
          <Grid item xs={10} sm={10} md={10} lg={10} xl={10}>
            <LoginButtonComponent
              loginGoogle={loginGoogle}
              logout={logout}
            ></LoginButtonComponent>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default HeaderComponent;
