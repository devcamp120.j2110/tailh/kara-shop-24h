// Component
import LoginModal from "../../LoginModal/LoginModalComponent";
import Snackbar from "../../SnackBar/SnackBarComponent";
// Material UI
import { Grid, IconButton, Badge, Button, Avatar } from "@mui/material";
// MUI style
import { styled } from "@mui/material/styles";
import { orange, red } from "@mui/material/colors";
import { grey } from "@mui/material/colors";
// Icon MUI
import LoginIcon from "@mui/icons-material/Login";
import LogoutIcon from "@mui/icons-material/Logout";
import NotificationsActiveIcon from "@mui/icons-material/NotificationsActive";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
// Hook
import { useState, useEffect } from "react";
// Redux
import { useSelector } from "react-redux";
// Router
import { useNavigate } from "react-router-dom";

function LoginButtonComponent(props) {
  // Navigate đến trang cart
  const navigate = useNavigate();
  const goToCartPage = () => {
    // Scroll đến đầu trang
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    navigate("/shopping-cart");
  };

  // State
  const [snackbar, setSnackBar] = useState({
    display: false,
    type: "warning",
    text: "",
  });
  const [openModalLogin, setOpenModalLogin] = useState(false);

  // Redux
  const user = useSelector((state) => state.user.user);
  const cartItems = useSelector((state) => state.cart.cartItems);
  const totalQuantity = cartItems.reduce(
    (total, quantity) => total + quantity.quantity,
    0
  );
  // Custom Button
  const ColorButtonLogin = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(orange[400]),
    backgroundColor: orange[400],
    "&:hover": {
      backgroundColor: orange[500],
    },
  }));
  const ColorButtonLogOut = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(red[400]),
    backgroundColor: red[400],
    "&:hover": {
      backgroundColor: red[500],
    },
  }));
  // Handle Snack Bar
  // Handle Open Modal
  const openModal = () => {
    setOpenModalLogin(true);
  };
  // Hàm logoutGG
  const logOutGoogle = () => {
    props.logout();
    setOpenModalLogin(false);
  };

  return (
    <Grid container>
      {/* Icon Group */}
      <Grid
        item
        xs={12}
        sm={12}
        md={12}
        lg={12}
        xl={12}
        sx={{
          display: "flex",
          justifyContent: "flex-end",
          alignItems: "center",
          boxSizing: "border-box",
        }}
      >
        <IconButton
          aria-label="notification"
          sx={{
            display: {
              xs: "none",
              sm: "none",
              md: "inline-flex",
              lg: "inline-flex",
              xl: "inline-flex",
            },
          }}
        >
          <Badge badgeContent={9} color="warning">
            <NotificationsActiveIcon sx={{ color: grey[900] }} />
          </Badge>
        </IconButton>
        <IconButton
          sx={{ marginRight: "1rem" }}
          onClick={goToCartPage}
          aria-label="cart"
        >
          <Badge badgeContent={totalQuantity} color="warning">
            <ShoppingCartIcon sx={{ color: grey[900] }} />
          </Badge>
        </IconButton>

        {/* Button Loggin*/}
        {user !== null ? (
          <>
            <Avatar
              sx={{
                width: { xs: 36, sm: 40, md: 40, lg: 40, xl: 40 },
                height: { xs: 36, sm: 40, md: 40, lg: 40, xl: 40 },
                marginLeft: 1,
                marginRight: 1,
              }}
              alt="avatar"
              src={user.photoURL}
            />
            <ColorButtonLogOut
              size="medium"
              variant="contained"
              endIcon={<LogoutIcon />}
              onClick={logOutGoogle}
              sx={{ fontWeight: "bold" }}
            >
              LogOut
            </ColorButtonLogOut>
          </>
        ) : (
          <>
            <ColorButtonLogin
              size="medium"
              variant="contained"
              endIcon={<LoginIcon />}
              onClick={openModal}
              sx={{ fontWeight: "bold" }}
            >
              Login
            </ColorButtonLogin>
            <LoginModal
              loginGoogle={props.loginGoogle}
              openModalLogin={openModalLogin}
              setOpenModalLogin={setOpenModalLogin}
              setSnackBar={setSnackBar}
            />
          </>
        )}
      </Grid>
      {/* Snackbar */}
      <Snackbar snackbar={snackbar} setSnackBar={setSnackBar}></Snackbar>
    </Grid>
  );
}

export default LoginButtonComponent;
