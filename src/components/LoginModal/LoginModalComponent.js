// Charkra UI
import {
  ChakraProvider,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormHelperText,
  Input,
  Link,
} from "@chakra-ui/react";
// Hook
import { useState, useEffect } from "react";
// Router
import { useNavigate } from "react-router-dom";
// Icon
import { BsGoogle, BsFacebook, BsTwitter, BsTelegram } from "react-icons/bs";
// Fetch API
import fetchApi from "../../fetchApi";
// Redux
import { useDispatch } from "react-redux";
import { update } from "../../redux/User/UserSlice";

function LoginModal({
  openModalLogin,
  setOpenModalLogin,
  loginGoogle,
  setSnackBar,
}) {
  // State
  const [loginInfo, setLoginInfo] = useState({
    email: "",
    password: "",
  });
  const [errorMessage, setErrorMessage] = useState(false);

  // Redux
  const dispatch = useDispatch();

  // Handle Email Change
  const handleChangeEmail = (e) => {
    setLoginInfo({ ...loginInfo, email: e.target.value });
  };
  // Handle Password Change
  const handleChangePassword = (e) => {
    setLoginInfo({ ...loginInfo, password: e.target.value });
  };
  // Đóng Modal
  const closeModal = () => {
    // reset State
    setLoginInfo({
      email: "",
      password: "",
    });
    setOpenModalLogin(false);
    setErrorMessage(false)
  };
  // Login GG
  const login = () => {
    loginGoogle();
  };
  //  Navigate
  const navigate = useNavigate();
  const goToSignUpPage = () => {
    // Đóng modal
    closeModal();
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    navigate("/sign-up");
  };
  // Hàm loggin bằng email và password
  const signInWithEmailAndPassword = () => {
    // validate
    let validateFormLogin = validate();
    if(validateFormLogin) {
      callApiCheckUser(loginInfo.email, loginInfo.password);
    }
  };
  // validate
  const validate = () => {
    if(loginInfo.email.trim() === "") {
      setErrorMessage(true);
      return false;
    }
    if(loginInfo.password.trim() === "") {
      setErrorMessage(true);
      return false;
    }
    return true;
  }
  // callApiCheckUser
  const callApiCheckUser = (email, password) => {
    // Fetch Api get customer with email and password
    fetchApi(
      "http://localhost:8080/customers/email-password" +
        "?email=" +
        email +
        "&password=" +
        password
    )
      .then((data) => {
        console.log(data);
        if (data.customer !== null) {
          // Change User
          dispatch(update(data.customer));
          // Đóng Modal
          setOpenModalLogin(false);
          // Hiện Snack Bar
          setSnackBar({
            display: true,
            type: "success",
            text: "Successfully Login",
          });
        } else {
          // Đăng nhập k thành công hiển thị error message
          setErrorMessage(true);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }
  return (
    <ChakraProvider>
      <Modal
        blockScrollOnMount={false}
        isOpen={openModalLogin}
        onClose={closeModal}
        size="xl"
        isCentered
      >
        <ModalOverlay
          bg="blackAlpha.300"
          backdropFilter="blur(6px) hue-rotate(180deg)"
        />
        <ModalContent>
          <ModalHeader></ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            {/* Email */}
            <FormControl>
              <FormLabel>Email</FormLabel>
              <Input
                placeholder="email@address.com"
                onChange={handleChangeEmail}
              />
              <FormErrorMessage>Sorry, your email or password are incorrect</FormErrorMessage> 
              {
                errorMessage ? <FormHelperText color="red.500">Sorry, your email or password are incorrect</FormHelperText>  : null
              } 
            </FormControl>
            {/* Password */}
            <FormControl mt={4}>
              <FormLabel>Password</FormLabel>
              <Input
                type="password"
                placeholder="Enter your password"
                onChange={handleChangePassword}
              />
              {
                errorMessage ? <FormHelperText color="red.500">Sorry, your email or password are incorrect</FormHelperText>  : null
              }
            </FormControl>
          </ModalBody>
          <ModalFooter>
            <FormControl>
              Don't have an account?
              <Link onClick={goToSignUpPage} color="teal.500" href="#" ml={1}>
                Sign up here
              </Link>
            </FormControl>
          </ModalFooter>
          {/* Sign In Normal */}
          <Button
            onClick={() => {
              signInWithEmailAndPassword();
            }}
            colorScheme="teal"
            mx={4}
            mb={6}
          >
            Sign In
          </Button>
          {/* Sign In Google */}
          <Button
            leftIcon={<BsGoogle />}
            colorScheme="red"
            mx={4}
            mb={3}
            onClick={login}
          >
            Sign in with Google
          </Button>
          {/* Sign In Facebook */}
          {/* <Button
            leftIcon={<BsFacebook />}
            colorScheme="facebook"
            mx={4}
            mb={3}
          >
            Sign in with Facebook
          </Button> */}
          {/* Sign In Twitter */}
          {/* <Button leftIcon={<BsTwitter />} colorScheme="twitter" mx={4} mb={3}>
            Sign in with Twitter
          </Button> */}
          {/* Sign In Telegram */}
          {/* <Button
            leftIcon={<BsTelegram />}
            colorScheme="telegram"
            mx={4}
            mb={3}
          >
            Sign in with Telegram
          </Button> */}
        </ModalContent>
      </Modal>
    </ChakraProvider>
  );
}

export default LoginModal;
