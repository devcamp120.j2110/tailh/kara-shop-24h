// Material UI
import { Grid } from "@mui/material";
// Component
import ProductFooter from "./ProductFooter/ProductFooter";
import ServiceFooter from "./ServicesFooter/ServiceFooter";
import SocialFooter from "./SocialFooter/SocialFooter";
import SupportFooter from "./SupportFooter/SupportFooter";

function FooterComponent() {
  return (
    <Grid
      sx={{ backgroundColor: "aliceblue", padding: {xs:"1rem 0",sm:"1rem 0",md:"3rem 0",lg:"4rem 0",xl:"4rem 0"} }}
      container
      justifyContent="center"
      className="sticky bottom-0"
    >
      <Grid item xs={8} sm={8} md={8} lg={8} xl={8}>
        <Grid container justifyContent="center">
          <Grid
            item
            xs={4}
            sm={4}
            md={4}
            lg={4}
            xl={4}
            sx={{ display: { xs: "none", sm: "none", md: "block" } }}
            align="center"
          >
            <ProductFooter></ProductFooter>
          </Grid>
          <Grid
            item
            xs={4}
            sm={4}
            md={4}
            lg={4}
            xl={4}
            sx={{ display: { xs: "none", sm: "none", md: "block" }}}
            align="center"
          >
            <ServiceFooter></ServiceFooter>
          </Grid>
          <Grid
            item
            xs={4}
            sm={4}
            md={4}
            lg={4}
            xl={4}
            sx={{ display: { xs: "none", sm: "none", md: "block" }}}
            align="center"
          >
            <SupportFooter></SupportFooter>
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
            <SocialFooter></SocialFooter>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default FooterComponent;
