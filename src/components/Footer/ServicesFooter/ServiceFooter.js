import { Typography, Box} from "@mui/material";

function ServiceFooter() {
  
  return (
    <Typography aligh="center" component="div">
      <Box sx={{ fontWeight: "500", m: 1 ,fontSize: 'h5.fontSize' }}>Dịch Vụ</Box>
      <Box sx={{ fontWeight: "regular", m: 1 }}>Thay Mới</Box>
      <Box sx={{ fontWeight: "regular", m: 1 }}>Phụ tùng - phụ kiện</Box>
      <Box sx={{ fontWeight: "regular", m: 1 }}>Chăm sóc</Box>
      <Box sx={{ fontWeight: "regular", m: 1 }}>Đặt Hẹn</Box>
      <Box sx={{ fontWeight: "regular", m: 1 }}>Bảo Trì</Box>
    </Typography>
  );
}

export default ServiceFooter;
