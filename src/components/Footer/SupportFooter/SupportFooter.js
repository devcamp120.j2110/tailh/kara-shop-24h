import { Typography, Box} from "@mui/material";

function SupportFooter() {
  return (
    <Typography component="div">
      <Box sx={{ fontWeight: "500", m: 1 ,fontSize: 'h5.fontSize' }}>Hỗ Trợ</Box>
      <Box sx={{ fontWeight: "regular", m: 1 }}>Help Center</Box>
      <Box sx={{ fontWeight: "regular", m: 1 }}>Contact Us</Box>
      <Box sx={{ fontWeight: "regular", m: 1 }}>Product Help</Box>
      <Box sx={{ fontWeight: "regular", m: 1 }}>Warranty</Box>
      <Box sx={{ fontWeight: "regular", m: 1 }}>Order Status</Box>
    </Typography>
  );
}

export default SupportFooter;
