import { Grid ,Box } from "@mui/material";
import Logo from "../../../assets/Logo/logo_transparent.png"
import FacebookIcon from '@mui/icons-material/Facebook';
import YouTubeIcon from '@mui/icons-material/YouTube';
import InstagramIcon from '@mui/icons-material/Instagram';
import TwitterIcon from '@mui/icons-material/Twitter';
import { useNavigate } from "react-router-dom";

function SocialFooter() {
  const navigate = useNavigate();
  const goToHomePage = () => {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    navigate("/");
  };
  return (
    <Grid container>
      <Grid item xs={12}  sm={12} md={12}>
          <Box onClick={goToHomePage}  sx={{width:"136px", height:"120px" , cursor:"pointer" , margin:"auto"}} >
            <img className="w-100 h-100" src={Logo} alt="logo-footer"/>
          </Box>  
      </Grid>
      <Grid sx={{display:"flex",justifyContent:"center"}} item xs={12} sm={12}>
          <FacebookIcon sx={{ cursor:"pointer"}} className="mx-1" fontSize="medium" />
          <YouTubeIcon sx={{ cursor:"pointer"}} className="mx-1" fontSize="medium" />
          <InstagramIcon sx={{ cursor:"pointer"}} className="mx-1" fontSize="medium" />
          <TwitterIcon sx={{ cursor:"pointer"}} className="mx-1" fontSize="medium" />
      </Grid>
    </Grid>
  );
}

export default SocialFooter;
