import { Typography, Box} from "@mui/material";
function ProductFooter() {
  return (
    <Typography component="div">
      <Box sx={{  fontWeight: "500", m: 1 ,fontSize: 'h5.fontSize' }}>Sản Phẩm</Box>
      <Box sx={{ fontWeight: "regular", m: 1 }}>Sản Phẩm 1</Box>
      <Box sx={{ fontWeight: "regular", m: 1 }}>Sản Phẩm 2</Box>
      <Box sx={{ fontWeight: "regular", m: 1 }}>Sản Phẩm 3</Box>
      <Box sx={{ fontWeight: "regular", m: 1 }}>Sản Phẩm 4</Box>
      <Box sx={{ fontWeight: "regular", m: 1 }}>Sản Phẩm 5</Box>
    </Typography>
  );
}

export default ProductFooter;
