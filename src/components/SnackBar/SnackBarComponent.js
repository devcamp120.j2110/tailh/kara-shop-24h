// Material UI
import { Snackbar, Alert, AlertTitle } from "@mui/material";
function SnackBar(props) {
  // Hàm xử lý khi đóng Snackbar
  const handleClose = () => {
    // reset state
    props.setSnackBar({
      display: false,
      type: "",
      text: "",
    });
  };
  // xác định vị trí của snackbar
  const vertical = "top";
  const horizontal = "right";

  return (
    <Snackbar
      open={props.snackBar?.display}
      autoHideDuration={2000}
      onClose={handleClose}
      anchorOrigin={{ vertical, horizontal }}
    >
      <Alert
        onClose={handleClose}
        variant="filled"
        severity={props.snackBar?.type}
        sx={{ width: "100%" }}
      >
        <AlertTitle className="text-capitalize fw-bold fs-5">
          {props.snackBar?.type} !!!
        </AlertTitle>
        <span className="fs-6 fw-bold">{props.snackBar?.text}</span>
      </Alert>
    </Snackbar>
  );
}

export default SnackBar;
