// Charkra UI
import {
  ChakraProvider,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  FormControl,
  FormLabel,
  Input,
  Text,
} from "@chakra-ui/react";
// Hook
import { useEffect, useState } from "react";
// Redux
import { useSelector, useDispatch } from "react-redux";
import { clearCart } from "../../redux/Cart/CartSlice";
// react router
import { useNavigate } from "react-router-dom";
// Call API
import fetchApi from "../../fetchApi";
function UserInfoModal(props) {
  // Redux
  const cartItems = useSelector((state) => state.cart.cartItems);
  const user = useSelector((state) => state.user.user);
  const dispatch = useDispatch();

  //  Navigate
  const navigate = useNavigate();
  const goToOrderSuccessPage = () => {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    navigate("/order-successful");
  };
  //  State
  const [userInfo, setUserInfo] = useState({});

  //   Handle Form
  const changeFullname = (e) => {
    setUserInfo({ ...userInfo, fullname: e.target.value });
  };
  const changePhoneNumber = (e) => {
    setUserInfo({ ...userInfo, phoneNumber: e.target.value });
  };
  const changeAddress = (e) => {
    setUserInfo({ ...userInfo, address: e.target.value });
  };
  const changeCity = (e) => {
    setUserInfo({ ...userInfo, city: e.target.value });
  };
  const changeCountry = (e) => {
    setUserInfo({ ...userInfo, country: e.target.value });
  };
  // hàm xử lý khi đóng modal
  const handleCloseModal = () => {
    // reset state
    setUserInfo({
      fullname: user.fullname,
      phoneNumber: user.phoneNumber,
      email: user.email,
      address: user.address,
      city: user.city,
      country: user.country,
    });
    props.setUserInfoModal(false);
  };
  //   Hàm Validate Dữ liệu
  const validate = () => {
    if (userInfo.fullname === "") {
      console.log("Bạn chưa nhập Họ Tên");
      return false;
    }
    if (userInfo.phoneNumber === "" || userInfo.phoneNumber === null) {
      console.log("Bạn chưa nhập Số Điện Thoại");
      return false;
    }
    if (isNaN(userInfo.phoneNumber)) {
      console.log("Bạn Vui Lòng Nhập Lại Số Điện Thoại");
      return false;
    }
    if (userInfo.address === "") {
      console.log("Bạn chưa nhập Địa Chỉ");
      return false;
    }
    if (userInfo.city === "") {
      console.log("Bạn chưa nhập Thành Phố");
      return false;
    }
    if (userInfo.country === "") {
      console.log("Bạn chưa nhập Quốc Gia");
      return false;
    }
    return true;
  };
  //Hàm Create Order
  const createOrder = () => {
    //Chỉnh Sửa thông tin khách hàng sau khi nhấn Check Out
    let validateUserInfo = validate();
    if (validateUserInfo) {
      console.log("Success");
      const customer = {
        method: "PUT",
        body: JSON.stringify(userInfo),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      };
      fetchApi("http://localhost:8080/customers/" + user._id, customer)
        .then((data) => {
          console.log(data);
          //Tạo Order Sau khi Sửa thành công User
          props.callApiCreateOrder(user._id, cartItems);
          //Đóng Modal
          handleCloseModal();
          //Clear Giỏ Hàng
          dispatch(clearCart());
          //Navigate Trang Order Successful
          goToOrderSuccessPage();
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      console.log("Fail");
    }
  };
  useEffect(() => {
    setUserInfo(user);
  }, [user]);
  return (
    <>
      <ChakraProvider>
        <Modal
          blockScrollOnMount={false}
          isOpen={props.userInfoModal}
          onClose={handleCloseModal}
          size="xl"
          isCentered
        >
          <ModalOverlay
            bg="blackAlpha.300"
            backdropFilter="blur(6px) hue-rotate(180deg)"
          />
          <ModalContent>
            <ModalHeader>
              <Text
                align="center"
                className="fw-bold"
                fontSize="4xl"
                color="blue.600"
              >
                Confirm your account
              </Text>
            </ModalHeader>
            <ModalCloseButton />
            <ModalBody pb={6}>
              {/* Full Name */}
              <FormControl>
                <FormLabel>Full Name</FormLabel>
                <Input
                  color="blue.600"
                  placeholder="Username"
                  _placeholder={{ opacity: 0.4, color: "inherit" }}
                  value={userInfo?.fullname}
                  onChange={changeFullname}
                />
              </FormControl>
              {/* Phone Number */}
              <FormControl mt={4}>
                <FormLabel>Phone Number</FormLabel>
                <Input
                  color="blue.600"
                  _placeholder={{ opacity: 0.4, color: "inherit" }}
                  placeholder="Phone Number"
                  value={userInfo?.phoneNumber}
                  onChange={changePhoneNumber}
                />
              </FormControl>
              {/* Email */}
              <FormControl mt={4}>
                <FormLabel>Email</FormLabel>
                <Input placeholder="Email" isDisabled value={userInfo?.email} />
              </FormControl>
              {/* Address */}
              <FormControl mt={4}>
                <FormLabel>Address</FormLabel>
                <Input
                  color="blue.600"
                  _placeholder={{ opacity: 0.4, color: "inherit" }}
                  placeholder="Address"
                  value={userInfo?.address}
                  onChange={changeAddress}
                />
              </FormControl>
              {/* City */}
              <FormControl mt={4}>
                <FormLabel>City</FormLabel>
                <Input
                  color="blue.600"
                  _placeholder={{ opacity: 0.4, color: "inherit" }}
                  placeholder="City"
                  value={userInfo?.city}
                  onChange={changeCity}
                />
              </FormControl>
              {/* Country */}
              <FormControl mt={4}>
                <FormLabel>Country</FormLabel>
                <Input
                  color="blue.600"
                  _placeholder={{ opacity: 0.4, color: "inherit" }}
                  placeholder="Country"
                  value={userInfo?.country}
                  onChange={changeCountry}
                />
              </FormControl>
            </ModalBody>
            <ModalFooter>
              {/* Confirm Button */}
              <Button colorScheme="blue" mr={3} onClick={createOrder}>
                Confirm
              </Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
      </ChakraProvider>
    </>
  );
}

export default UserInfoModal;
