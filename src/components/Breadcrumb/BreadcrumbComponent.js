// Material UI
import { Grid, Breadcrumbs, Link } from "@mui/material";
// MUI icons
import HomeIcon from "@mui/icons-material/Home";
import DirectionsCarIcon from '@mui/icons-material/DirectionsCar';
import FortIcon from '@mui/icons-material/Fort';
// Style CSS
import "./BreadcrumbStyle.css";

function BreadcrumbComponent(props) {
  return (
    <Grid justifyContent="center" container>
      <Grid className="d-flex" justifyContent="flex-start" item xs={8}>
        <Breadcrumbs aria-label="breadcrumb">
          <Link
            className="breadcrumb"
            sx={{
              display: "flex",
              alignItems: "center",
              fontSize: "h6.fontSize",
            }}
            color="#333"
            href="/"
          >
            <HomeIcon sx={{ mr: 0.5 }} fontSize="inherit" />
            Home
          </Link>

          <Link
            className="breadcrumb"
            sx={{
              display: "flex",
              alignItems: "center",
              fontSize: "h6.fontSize",
            }}
            color="#333"
            href="/allproduct"
            aria-current="page"
          >
            <FortIcon sx={{ mr: 0.5 }} fontSize="inherit" />
            Product List
          </Link>
            {props.productDetail ? <Link
            className="breadcrumb"
            sx={{
              display: "flex",
              alignItems: "center",
              fontSize: "h6.fontSize",
            }}
            color="#333"
            href="#"
            aria-current="page"
          >
            <DirectionsCarIcon sx={{ mr: 0.5 }} fontSize="inherit" />
            Product Detail
          </Link> : null}
        </Breadcrumbs>
      </Grid>
    </Grid>
  );
}

export default BreadcrumbComponent;
