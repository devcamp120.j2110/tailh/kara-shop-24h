// Import Slice
import { createSlice } from "@reduxjs/toolkit";
// Tạo initialState
const initialState = {
  cartItems: [],
};
// Tạo Slice
export const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    // Hàm Add To Cart
    add: (state, action) => {
      // Tìm trong cartItems
      const cartItem = state.cartItems.find(
        (item) => item.product._id === action.payload.product._id
      );
      // Nếu tìm được
      // TH1 : Tăng quantity lên 1 nếu không truyền quantity vào
      // TH2 : Tăng quantity lên 1 số truyền vào action.payload
      if (cartItem) {
        cartItem.quantity =
          action.payload.quantity !== undefined
            ? cartItem.quantity + action.payload.quantity
            : cartItem.quantity + 1;
      } else {
        // Nếu không tìm được thì add vào cartItems
        state.cartItems.push({
          product: action.payload.product,
          quantity: action.payload.quantity || 1,
        });
      }
    },
    // Hàm Remove from cart
    remove: (state, action) => {
      // tìm item trong cart
      const cartItem = state.cartItems.find(
        (item) => item.product._id === action.payload.product._id
      );
      // sau khi tìm đc thì giảm quantity đi 1 đơn vị (quantity nhỏ nhất là 1)
      if (cartItem && cartItem.quantity > 1) {
        cartItem.quantity -= 1;
      }
    },
    // Hàm xoá cart Item
    deleteCartItem: (state, action) => {
      // input index
      state.cartItems.splice(action.payload, 1);
    },
    // Hàm xoá hết tất cả các cartItem
    clearCart: (state, action) => {
      state.cartItems = [];
    },
  },
});

// Export những action từ cartSlice reducers
export const { add, remove, deleteCartItem, clearCart } = cartSlice.actions;
// Export Reducer
export default cartSlice.reducer;
