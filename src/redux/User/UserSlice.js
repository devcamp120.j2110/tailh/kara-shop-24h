// Import Slice
import { createSlice } from "@reduxjs/toolkit";
// Tạo initialState
const initialState = {
  user: null,
};
// Tạo Slice
export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    update: (state, action) => {
      state.user = action.payload;
    },
  },
});

// Export những action từ cartSlice reducers
export const { update } = userSlice.actions;
// Export Reducer
export default userSlice.reducer;
