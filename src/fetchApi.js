const fetchApi = async (paramUrl, paramOption = {}) => {
  const response = await fetch(paramUrl, paramOption);
  const responseData = await response.json();
  return responseData;
};

export default fetchApi;