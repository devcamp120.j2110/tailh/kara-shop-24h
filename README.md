# Tạo Component Chính 
    - Header : Chứa Logo , Icon , Button Login
    - Footer : Chứa Logo
    - Content : Gồm  HomePage , All Product , Product Detail , Cart , Sign Up Form

### Content
# HomePage
    - Call Api lưu vào state ở App.js lấy tất cả product sau đó set limit để giới hạn số lượng
    - Từ Data tạo Carousel và Card
    - Cuối Cùng là 1 nút ViewAll dẫn đến trang All Product
# All Product
    - Truyền Dữ liệu từ App.js vào Render ra Tất cả các thẻ + Phân Trang
    - Tạo Category search dữ liệu từ back-end:
        + Có thể search chung or độc lập các trường ( name , type , color , Price MIN, Price Max)
        + Trường Price Min , Price max chỉ có thể nhập số
        + Khi bất kỳ trường nào có dữ liệu sẽ hiển thị nút clear
        + Nhấn vào nút clear sẽ xoá hết dữ liệu hiện tại trong các ô input
# Product Detail
    - Carousel + Info + Description + Sản Phẩm Liên Quan 
    - Lọc sản phẩm liên quan (cùng type) từ backend và render ra trình duyệt
    - Có 1 nút tăng số lượng sản phẩm vào giỏ hàng, sau khi tăng bấm ADD sẽ add đúng với số lượng đã tăng hiện tại
# Cart
    - Tất cả những phần liên Quan đến Cart or User đều dùng RTK (Redux Tookit)
    - Khi Bấm Add To Cart ở mọi trang thì đều add đc vào giỏ hàng
    - Trong trang Detail có thể add sản phẩm theo số lượng chỉ định
    - Update số lượng và giá ở trang Shopping Cart (Phần Payment Summary)
    - Số Lượng hiện trên icon cũng đc update
    - Nút Check Out: 
        1 > TH chưa đăng Nhập thì hiện SnackBar yêu cầu đăng nhập
        2 > TH đăng Nhập thì modal thông tin ng dùng(call Api) , Bấm confirm để xác nhận gửi đơn đồng thời sửa thông tin user trên DB
    - Có thể tăng và xoá sản phẩm trong phần Order 
        + Tăng vô hạn nhưng giảm chỉ giảm đến 1

### Loggin
# Loggin Firebase
    - Khi Loggin thì chạy hàm createCustomer để tạo user trên DB , kiểm tra user đã tồn tại hay chưa thông qua email
       + Nếu user đã tồn tại thì setState lưu customer đó
       + Nếu user chưa tồn tại thì create user và lưu vào DB
# Loggin thông thường
    - Khi Loggin check email và password , nếu cả 2 chính xác thì lưu user vào store thay đổi avatar , nếu k chính xác thì hiển thị lỗi ở input
### Sign Up
    - Nhấn vào Sign Up sẽ nhảy sang trang đăng ký
    - Điền đầy đủ các trường để tạo account
    - Password & Confirm Password phải giống nhau và có từ 7 - 15 chữ cố trong đó chữ đầu tiên phải viết hoa
    - Trường password & confirm password đều có nút để hiển mật khẩu ẩn
    - Check Email nếu email đã tồn tại thì k tạo đc account
